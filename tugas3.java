import java.sql.SQLOutput;
import java.util.Scanner;

public class tugas3 {

        public static int volBangun (int p, int l, int t){
            return p*l*t;
        }
        public static float volBangun (float pi, float r){
            return 4*pi*(r*r*r)/3;
        }
        public static float volBangun (float r, float t, float pi){
            return pi*r*r*t;
        }

        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            first:
            do {
                System.out.println("MENU");
                System.out.println("1. Volume Balok");
                System.out.println("2. Volume Bola");
                System.out.println("3. Volume Tabung");
                System.out.println("4. EXIT");
                System.out.print("Input nomor: ");
                int inputPilihan = input.nextInt();
                switch (inputPilihan) {
                    case 1:
                        System.out.print("Panjang = ");
                        int p = input.nextInt();
                        System.out.print("Lebar = ");
                        int l = input.nextInt();
                        System.out.print("Tinggi = ");
                        int t = input.nextInt();
                        System.out.println("Volume Balok = " + volBangun(p, l, t));
                        break;
                    case 2:
                        System.out.print("Phi = ");
                        float pi = input.nextFloat();
                        System.out.print("Jari-jari = ");
                        float r = input.nextFloat();
                        System.out.println("Pengurangan = " + volBangun(pi, r));
                        break;
                    case 3:
                        System.out.print("Phi = ");
                        float pi2 = input.nextFloat();
                        System.out.print("Jari-jari = ");
                        float r2 = input.nextFloat();
                        System.out.print("Tinggi = ");
                        float t2 = input.nextFloat();
                        System.out.println("Perkalian = " + volBangun(r2, t2, pi2));
                        break;
                    case 4:
                        System.out.println("EXIT...");
                        break first;
                    default:
                        System.out.println("Pilihan tidak tersedia");
                }
            } while (true);
            input.close();
        }
    }
