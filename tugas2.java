import java.util.Scanner;

public class tugas2 {
    static int x,y;

    public void pertambahan (int x,int y){
        int hasil = x + y;
        System.out.println("pertambahan = " + hasil);
    }
    public void pengurangan (int x,int y){
        int hasil = x - y;
        System.out.println("pengurangan = " + hasil);
    }
    public void perkalian (int x,int y){
        int hasil = x * y;
        System.out.println("perkalian = " + hasil);
    }
    public void pembagian (float x,float y){
        float hasil = x / y;
        System.out.println("pembagian = " + hasil);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        tugas2 methodOperator = new tugas2();

        System.out.println("KALKULATOR");
        System.out.println("1. Pertambahan");
        System.out.println("2. Pengurangan");
        System.out.println("3. Perkalian");
        System.out.println("4. Pembagian");
        System.out.println("5. EXIT");

        int operator;

        do {
            System.out.print("Input Nomor = ");
            operator = input.nextInt();
            if (operator > 0 && operator < 5) {
                System.out.print("Bilangan 1 = ");
                x = input.nextInt();
                System.out.print("Bilangan 2 = ");
                y = input.nextInt();
            }

                switch (operator) {
                    case 1:
                        methodOperator.pertambahan(x, y);
                        break;
                    case 2:
                        methodOperator.pengurangan(x, y);
                        break;
                    case 3:
                        methodOperator.perkalian(x, y);
                        break;
                    case 4:
                        methodOperator.pembagian(x, y);
                        break;
                    default:
                        if(operator == 5){
                            System.out.println("Exit Menu...");
                            break;
                        }
                        else {
                            System.out.println("Input nomor tidak ditemukan");
                        }
                }
            }
            while (operator != 5) ;

    }


}
