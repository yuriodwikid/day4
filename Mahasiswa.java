import java.util.Scanner;

    public class Mahasiswa {
        String nama;
        String jurusan;
        int nilaiFisika;
        int nilaiKimia;
        int nilaiBiologi;
        Mahasiswa() {
            System.out.println("Konstruktor tanpa parameter");
        }

        Mahasiswa(String nama, String jurusan) {
            System.out.println("Konstruktor dengan 2 parameter");
            this.nama = nama;
            this.jurusan = jurusan;
        }

        Mahasiswa(String nama, String jurusan, int nilaiFisika, int nilaiKimia, int nilaiBiologi) {
            System.out.println("Konstruktor dengan 5 parameter");
            this.nama = nama;
            this.jurusan = jurusan;
            this.nilaiFisika = nilaiFisika;
            this.nilaiKimia = nilaiKimia;
            this.nilaiBiologi = nilaiBiologi;
        }

        public static void main(String[] args) {
            first:
            do {
                Scanner input = new Scanner(System.in);
                System.out.println("MENU");
                System.out.println("1. Konstruktor tanpa parameter");
                System.out.println("2. Konstruktor dengan 3 parameter");
                System.out.println("3. Konstruktor dengan 5 parameter");
                System.out.println("4. EXIT");
                System.out.print("Input nomor: ");
                int choice = input.nextInt();

                switch (choice){
                    case 1:
                        Mahasiswa obj = new Mahasiswa();
                        if (obj.nama == null && obj.jurusan == null) {
                            System.out.print("Default " + "Default \n");
                        }
                        System.out.println(obj.nilaiFisika + " " + obj.nilaiKimia + " " + obj.nilaiBiologi + "\n");
                        break;
                    case 2:
                        Mahasiswa obj2 = new Mahasiswa("Steve", "IT");
                        System.out.print(obj2.nama + " " + obj2.jurusan + "\n" + obj2.nilaiFisika + " " + obj2.nilaiKimia + " " + obj2.nilaiBiologi + "\n");
                        break;
                    case 3:
                        Mahasiswa obj3 = new Mahasiswa("Steve","IT",7,8,9);
                        System.out.print(obj3.nama + " " + obj3.jurusan + "\n" + obj3.nilaiFisika + " " + obj3.nilaiKimia + " " + obj3.nilaiBiologi + "\n");
                        break;
                    case 4:
                        System.out.println("Anda keluar dari menu pemilihan");
                        break first;
                    default:
                        System.out.println("Pilih menu 1-4");
                }

            } while (true);
        }
    }

